import React from 'react';
import './Button.css';

const Button = function({ text }) {
  return (
    <button className='Button'>{text}</button>
  );
}

export default Button;
