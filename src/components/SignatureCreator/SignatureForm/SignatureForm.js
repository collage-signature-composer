import React from 'react';
import Input from '../../Forms/Input/Input';
import Button from '../../Forms/Button/Button';
import CopyOnClick from '../../CopyOnClick/CopyOnClick';

const SignatureForm = function({ onChange, onSubmit, className }) {
  return (
    <form className={className} onSubmit={onSubmit}>
      <Input label='Name' type='text' autoFocus={true} onChange={onChange} />
      <Input label='Position' type='text' onChange={onChange} />
      <CopyOnClick target='.SignaturePreview'>
        <Button text='Copy' />
      </CopyOnClick>
    </form>
  );
}

export default SignatureForm;
